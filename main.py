from pprint import pprint
from sys import argv, exit
import datetime
import string
import hashlib
import json
import os
import sys
import io
import random
import re
import subprocess
import time

# installed with pip
import genanki
# import srt

ls = os.listdir


class CustomNote(genanki.Note):
    @property
    def guid(self):
        if not hasattr(self, '_guid') or self._guid is None:
            return genanki.util.guid_for(self.fields[:3])  # model, hint, Q
        return self._guid


def read(path, is_anki_data=False, read_as_text=False, **kwargs):
    with open(path, 'r', encoding='utf-8') as f:
        if is_anki_data:
            try:
                info = json.load(f)

                # generate a new ID if it is missing
                id_key = path.split('/')[0] + '_id'
                id_value = info.get(id_key)
                if id_value is None:
                    info[id_key] = create_id() # hardcode the id
                    write(path, info) # rewrite the file to store the newly generated id

                return info
            except Exception as e:
                print('=' * 80)
                print(f'[ERROR] {path}')
                print('=' * 80)
                raise e
        if path.endswith('.json'):
            return json.load(f)
        elif path.endswith('.srt'):
            return '\n'.join(row.content for row in srt.parse(f.read())).lower()
        else:
            return f.read()


def write(path, data, *, indent=4, **kwargs):
    if path.endswith('.json'):
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(data, f, indent=indent, sort_keys=True)
    else:
        raise ValueError('the file extension should be "json"')


def get_files(path, *, extension='', **kwargs):
    return sorted(os.path.join(dp, f)
        for dp, dn, fn in os.walk(os.path.expanduser(path))
            for f in fn if extension == '' or f.rpartition('.')[2] == extension)


def replace_pairs(text, pairs):
    for pair in pairs:
        text = text.replace(*pair)

    return text


def shell(cmd, **kwargs):
    return subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding='utf-8').stdout.strip()


def create_all_models():
    models = {}
    for fn in get_files('model'):
        model = create_model(fn)
        models[model.name] = model

    return models


def create_all_decks(models, force_reubild_decks):
    shell('mkdir output')

    decks = {}
    for fn in get_files('deck', extension='json'):
        if '.ipynb_checkpoints' in fn or fn.endswith('_DATA.json'):
            continue

        # if not '/test/' in fn: continue

        # print(fn)
        info = read(fn, is_anki_data=True)

        # sort answers
        for i, (meta, Q, *A) in enumerate(info['data']):
            if A and isinstance(A[0], list):
                info['data'][i] = [meta, Q, sorted(A[0])]

        # sort the rows by the (model, hint, question)
        info['data'] = sorted(info['data'], key=lambda row: (row[0].get('model'), row[0].get('hint', info.get('hint')), row[1]))

        prev_hash = info.pop('hash', None)
        new_hash = get_hash(info)
        if force_reubild_decks or prev_hash != new_hash:
            deck, info = create_deck(info, models)
            info['hash'] = get_hash(info)  # recompute hash because it could've changed in create_deck()
            write(fn, info)  # rewrite the .json file

            decks[deck.deck_id] = deck

            fn_out = replace_pairs(fn, (
                ('deck', 'output'),
                ('.json', '.apkg')))

            dn = fn_out.rpartition('/')[0]

            os.makedirs(dn, exist_ok=True)
            deck.write_to_file(fn_out)

    return decks


def create_model(fn):
    return genanki.Model(**read(fn, is_anki_data=True))


def create_deck(info, models):
    deck = genanki.Deck(info['deck_id'], info['name'])
    deck_hint = info.get('hint', '')

    for i, row in enumerate(info['data']):
        meta, Q, *A = row
        if isinstance(A, list) and len(A) >= 1:
            A = A[0]

        guid = meta.get('guid')
        model = meta.get('model')
        hint = meta.get('hint', deck_hint)
        hint_and_hr = f'{hint}<hr>' if hint else ''

        if model == 'QA':
            model_type = models.get('QA')
            front = f'{hint_and_hr}{Q}<hr>[{len(A)}]'
            back = f'{hint_and_hr}{Q}<hr>' + '<br>'.join(A)
            fields = [model, hint, Q, front, back]
        elif model == 'MCQ':
            model_type = models.get('MCQ')
            front = []
            correct = []
            wrong = []
            for answer in A:
                if answer.startswith('*'):
                    answer = answer[1:]
                    correct.append(answer)
                else:
                    wrong.append(answer)

                front.append(answer)

            front = f'{hint_and_hr}{Q}<hr><ul id="shuffle"><li>' + '</li><li>'.join(front) + '</li></ul>'
            back = f'{hint_and_hr}{Q}<hr>Correct:<br>' + '<br>'.join(correct) + '<hr>' + '<br>'.join(wrong)
            fields = [model, hint, Q, front, back]
        elif model == 'Cloze':
            model_type = models.get('Cloze')
            if A:
                cloze = f'{hint_and_hr}{Q}<hr>' + '<br>'.join('{{' + f'c{i}::{answer}' + '}}' for i, answer in enumerate(A, start=1))
            else:
                cloze = f'{hint_and_hr}{Q}'
                Q = re.sub(r'{{c\d+::([^:}]+)(::[^}]+)?}}', r'\1', Q)
            fields = [model, hint, Q, cloze]
        else:
            raise ValueError(f'row = {repr(row)}')

        if guid:
            note = CustomNote(guid=guid, model=model_type, fields=fields)
        else:
            note = CustomNote(model=model_type, fields=fields)
            info['data'][i][0]['guid'] = note.guid

        deck.add_note(note)

    return deck, info


def create_id():
    return int(str(datetime.datetime.now().timestamp()).replace('.', ''))


def get_hash(x):
    x.pop('hash', None)
    return hashlib.sha1(str(sorted(x.items())).encode('utf-8')).hexdigest()


def main():
    force_reubild_decks = (len(argv) >= 2 and argv[1] == 'force')

    models = create_all_models()
    decks = create_all_decks(models, force_reubild_decks=force_reubild_decks)

    print(f'Models [{len(models)}]:')
    for model in sorted(models.values(), key=lambda x: x.name):
        print(f'  {model.name}')

    print(f'\nDecks [{len(decks)}]:')
    for deck in sorted(decks.values(), key=lambda x: x.name):
        print(f'  {deck.name}')


if __name__ == '__main__': main()
