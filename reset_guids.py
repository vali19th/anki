from pprint import pprint
import json
import os

ls = os.listdir


def main():
    for g in ls('deck'):
        for fn in ls(f'deck/{g}'):
            if fn.split('.')[-1] != 'json':
                continue

            path = f'deck/{g}/{fn}'
            # print(path)
            x = read(path)
            d = x['data']
            for i, di in enumerate(d):
                if (di[0].get('model') not in ('MCQ', 'QA', 'Cloze')
                    or not di[0].get('guid')
                    or not isinstance(di[1], str)
                    or (len(di) > 2 and not isinstance(di[2], list))
                    or (len(di) <= 2 and di[0].get('model') == 'QA')
                    or (len(di) > 2 and len(di[2]) == 0)
                    or (len(di) > 2 and di[2][0] == '')
                ):
                    print(path)
                    print(di)

                # di[0].pop('guid', None)
                # m = di[0].get('model')
                #
                # if m == 'LIST':
                #     di[0]['model'] = 'QA'
                #
                # if di[0]['model'] == 'QA' and isinstance(di[2], str):
                #     di[2] = [di[2]]
                #
                # d[i] = di

            #
            # x['data'] = d
            # pprint(x['data'])
            # write(path, x)



def read(path, **kwargs):
    with open(path, 'r', encoding='utf-8') as f:
        return json.load(f)


def write(path, data, *, indent=2, **kwargs):
    with open(path, 'w', encoding='utf-8') as f:
        json.dump(data, f, indent=indent, sort_keys=True)


if __name__ == '__main__':
    main()
