import ast
from pprint import pprint
import json
import os
import re


def main():
    deck = {'name': 'All::Medicine', "id": 1651943609169935, 'data': []}
    for fn in ls('tmp/adda/'):
        hint = fn.split('.')[0]
        md = read(f'tmp/adda/{fn}')
        # print(md)

        parsed = md
        parsed = re.sub(r'^#', r'\n#', parsed)
        parsed = re.sub(r'\n- ', r'", "* ', parsed)
        parsed = re.sub(r'# (.*)', r'["\1"]', parsed)
        parsed = [ast.literal_eval(item) for item in parsed.split('\n') if item != '']
        parsed = [prepare_item(item, hint) for item in parsed]

        # pprint(parsed)
        deck['data'].extend(parsed)

    pprint(deck)

    write('deck/adda/medicine.json', deck)



def prepare_item(item, hint):
    if len(item) == 1:
        return [{'model': 'Cloze', 'hint': hint}, *item]
    else:
        return [{'model': 'QA', 'hint': hint}, item[0], item[1:]]


def ls(path):
	return sorted(os.listdir(path))


def read(path, **kwargs):
    with open(path, 'r', encoding='utf-8') as f:
        if path.endswith('json'):
            return json.load(f)
        else:
            return f.read()


def write(path, data, *, indent=2, **kwargs):
    with open(path, 'w', encoding='utf-8') as f:
        json.dump(data, f, indent=indent, sort_keys=True)


if __name__ == '__main__':
    main()
