from pprint import pprint
from sys import argv, exit


def main():
    with open('WIP/duolingo', 'r') as f:
        x = f.read().split()

    with open('WIP/spanish_duolingo', 'r') as f:
        x2 = [e.split(',') for e in f.read().split('\n') if e]

    pprint(x)
    pprint(x2)

    result = dict.fromkeys(x)
    for a, b in x2:
        if a in result:
            result[a] = b
        elif b in result:
            result[b] = a
        else:
            print(a,b)

    # pprint(result)

    result = {k: v for k,v in result.items() if v is None}
    pprint(result)

    print(len(set(result.values())))


if __name__ == '__main__': main()
