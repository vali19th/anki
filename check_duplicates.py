from pprint import pprint
from sys import argv, exit
from collections import defaultdict
import json
import os


def main():
    files = get_files('deck')
    for f in files:
        if 'spanish' not in f: continue
        if '_DATA' in f: continue

        duplicates = defaultdict(list)
        data = read_json(f)['data']
        for xi in data:
            duplicates[xi[0]].append(xi[1:])

        duplicates = {k:v for k,v in duplicates.items() if len(v) >= 2}
        if duplicates:
            print('\n')
            print('='*50)
            print(f)
            pprint(duplicates)



def read_json(path, **kwargs):
    with open(path, 'r', encoding='utf-8') as f:
        return json.load(f)


def get_files(path, *, extension='', **kwargs):
    return sorted(os.path.join(dp, f)
        for dp, dn, fn in os.walk(os.path.expanduser(path))
            for f in fn if extension == '' or f.rpartition('.')[2] == extension)


if __name__ == '__main__': main()
